import React from 'react';
import {
  BrowserRouter as HashRouter,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { TOKEN } from './constants';
import Home from './pages/Home';
import Favorites from './pages/Favorites/Favorites';
import Error404 from './pages/Error404';
import Header from './components/organisms/Header/Header';
import './styles/normalize.css';
import Register from './templates/Register/Register';
import Login from './templates/Login/Login';

let audio;

export const createAudioPreview = url => {
  audio && pauseAudio(audio);
  audio = new Audio(url);
  playAudio(audio);
};

export const playAudio = audio => {
  audio.play();
};

export const pauseAudio = () => {
  audio.pause();
};

function App() {
  return (
    <HashRouter>
      <Header />
      <Switch>
        <Route exact path="/" component={Login}>
          {TOKEN && <Redirect to="/home" />}
        </Route>
        <Route exact path="/register" component={Register}>
          {TOKEN && <Redirect to="/home" />}
        </Route>
        <Route path="/home" component={Home} />
        <Route exact path="/favorites" component={Favorites} />
        <Route path="*" component={Error404} />
      </Switch>
    </HashRouter>
  );
}

export default App;
