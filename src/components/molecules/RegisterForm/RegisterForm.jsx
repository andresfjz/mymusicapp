import React from 'react';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import Button from '../../atoms/Button/Button';
import {
  EMAIL_PATTERN,
  NAME_PATTERN,
  PASSWORD_PATTERN,
} from '../../../constants';
import './RegisterForm.scss';

const RegisterForm = () => {
  const {
    register,
    formState: { errors },
    handleSubmit,
    watch,
  } = useForm();

  const onSubmit = data => {
    console.log(JSON.stringify(data));
  };

  return (
    <form className="form" onSubmit={handleSubmit(onSubmit)}>
      <h3 className="form__title">Register your account</h3>
      <input
        {...register('name', { required: true, pattern: NAME_PATTERN })}
        className="form__input"
        type="text"
        placeholder="Enter your name"
      />
      {errors.name?.type === 'required' && (
        <p className="form__error">Please specify your name.</p>
      )}
      {errors.name?.type === 'pattern' && (
        <p className="form__error">
          Name only must have letters and blank spaces.
        </p>
      )}
      <input
        {...register('email', { required: true, pattern: EMAIL_PATTERN })}
        className="form__input"
        type="email"
        placeholder="Enter your email"
      />
      {errors.email?.type === 'required' && (
        <p className="form__error">Please specify your email.</p>
      )}
      {errors.email?.type === 'pattern' && (
        <p className="form__error">This is not a valid email.</p>
      )}
      <input
        {...register('password', { required: true, pattern: PASSWORD_PATTERN })}
        className="form__input"
        type="password"
        placeholder="Enter your password"
        autoComplete="off"
      />
      {errors.password?.type === 'required' && (
        <p className="form__error">Please specify your password.</p>
      )}
      {errors.password?.type === 'pattern' && (
        <p className="form__error">
          Password must have 8 characters, lowercase and uppercase letters and a
          symbol.
        </p>
      )}
      <input
        {...register('confirmPassword', {
          required: true,
          validate: value => value === watch('password'),
        })}
        className="form__input"
        type="password"
        placeholder="Confirm your password"
        autoComplete="off"
      />
      {errors.confirmPassword?.type === 'validate' && (
        <p className="form__error">Passwords doesn't match.</p>
      )}
      <Button myStyle="primary" type="submit" text="Register" />
      <Link to="/">
        <Button myStyle="secondary" text="Go back" />
      </Link>
    </form>
  );
};

export default RegisterForm;
