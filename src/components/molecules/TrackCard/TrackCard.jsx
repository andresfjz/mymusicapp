import React, { useEffect, useState } from 'react';
import {
  addTrackToUserLibrary,
  checkIsTrackInUserLibrary,
  removeTrackFromUserLibrary,
} from '../../../services/spotifyApi';
import Button from '../../atoms/Button/Button';
import { createAudioPreview, pauseAudio } from '../../../App';
import './TrackCard.scss';

const TrackCard = ({ track }) => {
  const [isSaved, setIsSaved] = useState(false);
  const [isPlaying, setIsPlaying] = useState(false);

  useEffect(() => {
    if (track.id) {
      const requestCheckIsTrackInUserLibrary = async () => {
        const response = await checkIsTrackInUserLibrary(track.id);
        setIsSaved(response);
      };
      requestCheckIsTrackInUserLibrary();
    }
  }, [track.id]);

  let showArtists;

  if (track.artists) {
    let myArtists = [];

    track.artists.forEach(artist => {
      myArtists.push(artist.name);
    });

    myArtists.length > 2
      ? (showArtists = `${myArtists[0]}, ${myArtists[1]} and ${
          myArtists.length - 2
        } more...`)
      : (showArtists = myArtists.join(', '));
  } else {
    showArtists = 'No artists';
  }

  if (track) {
    return (
      <>
        {/* {(isLoading && <Loader />) || ( */}
        <article className="card">
          {track.album.images[1].url && (
            <img
              className="card__cover"
              src={track.album.images[1].url}
              alt="Album cover"
            />
          )}
          <div className="card__data">
            <h3 className="card__name">{track.name}</h3>
            <p className="card__artist">{showArtists}</p>
            <p className="card__album">{track.album.name}</p>
            <div className="card__buttons">
              {track.preview_url &&
                ((!isPlaying && (
                  <Button
                    className="card__preview"
                    action={() => {
                      createAudioPreview(track.preview_url);
                      setIsPlaying(true);
                    }}
                    text="Play Preview"
                  />
                )) || (
                  <Button
                    className="card__preview"
                    action={() => {
                      pauseAudio();
                      setIsPlaying(false);
                    }}
                    text="Stop preview"
                  />
                ))}
              {!isSaved ? (
                <Button
                  className="card__save"
                  action={() => {
                    addTrackToUserLibrary(track.id);
                    setIsSaved(true);
                  }}
                  text="Add"
                />
              ) : (
                <Button
                  className="card__save"
                  action={() => {
                    removeTrackFromUserLibrary(track.id);
                    setIsSaved(false);
                  }}
                  text="Remove"
                />
              )}
            </div>
            <a
              className="card__link"
              href={track.external_urls.spotify}
              target="_blank"
              rel="noreferrer"
            >
              Show this on Spotify
            </a>
          </div>
        </article>
        {/* )} */}
      </>
    );
  } else {
    return <></>;
  }
};

export default TrackCard;
