import React, { Suspense } from 'react';
import useNearScreen from '../../../hooks/useNearScreen';
import TrackLoader from '../../atoms/TrackLoader/TrackLoader';
import './TrackCard.scss';

const TrackCard = React.lazy(() => import('./TrackCard'));

export default function LazyTrack({ track }) {
  const { isNearScreen, elementRef } = useNearScreen({ distance: '0px' });

  return (
    <div style={{ width: '100%' }} ref={elementRef}>
      <Suspense
        fallback={
          <article className="card">
            <TrackLoader />
          </article>
        }
      >
        {isNearScreen ? (
          <TrackCard track={track} />
        ) : (
          <article className="card">
            <TrackLoader />
          </article>
        )}
      </Suspense>
    </div>
  );
}
