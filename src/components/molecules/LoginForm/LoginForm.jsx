import React from 'react';
import Button from '../../atoms/Button/Button';
import { handleAuth } from '../../../services/spotifyAuth';
import './LoginForm.scss';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { EMAIL_PATTERN } from '../../../constants';

const LoginForm = () => {
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm();

  const onSubmit = data => {
    console.log(JSON.stringify(data));
  };

  return (
    <form className="form" onSubmit={handleSubmit(onSubmit)}>
      <h3 className="form__title">Log in to your account</h3>
      <input
        {...register('email', { required: true, pattern: EMAIL_PATTERN })}
        className="form__input"
        type="text"
        placeholder="Enter your email"
      />
      {errors.email?.type === 'required' && (
        <p className="form__error">Please specify your email.</p>
      )}
      {errors.email?.type === 'pattern' && (
        <p className="form__error">This is not a valid email.</p>
      )}
      <input
        {...register('password', { required: true })}
        className="form__input"
        type="password"
        placeholder="Enter your password"
      />
      {errors.password?.type === 'required' && (
        <p className="form__error">Please specify your password.</p>
      )}
      <Button myStyle="primary" type="submit" text="Log In" />
      <Button
        myStyle="spotify"
        action={handleAuth}
        text="Log in with Spotify"
      />
      <Link to="/register">
        <Button myStyle="secondary" text="Register an account" />
      </Link>
    </form>
  );
};

export default LoginForm;
