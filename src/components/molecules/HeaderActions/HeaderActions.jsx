import React /*, { useEffect, useState }*/ from 'react';
import Button from '../../atoms/Button/Button';
import { deleteMyToken } from '../../../services/spotifyAuth';
// import { getUserProfileData } from '../../../services/spotifyApi';
import './HeaderActions.scss';
import { useSpotifyApi } from '../../../hooks/useSpotifyApi';
import { PROFILE_DATA_ENDPOINT } from '../../../constants';

const HeaderActions = () => {
  // const [userName, setUserName] = useState('');
  // const [userPicture, setUserPicture] = useState('');

  let { data } = useSpotifyApi(PROFILE_DATA_ENDPOINT, 'GET');

  // useEffect(() => {
  //   const requestUserProfileData = async () => {
  //     const userProfileData = await getUserProfileData();
  //     setUserName(userProfileData.display_name);
  //     setUserPicture(userProfileData.images[0].url);
  //   };
  //   requestUserProfileData();
  // }, []);

  return (
    <div className="header__actions">
      <div className="header__profile">
        <img
          className="header__picture"
          src={data?.images[0].url}
          alt={data?.display_name}
        />
        <h4 className="header__name">{data?.display_name}</h4>
      </div>
      <Button action={deleteMyToken} text="Log Out" />
    </div>
  );
};

export default HeaderActions;
