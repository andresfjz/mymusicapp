import React from 'react';
import { NavLink } from 'react-router-dom';
import './Nav.scss';

const Nav = () => {
  return (
    <nav className="nav">
      <NavLink
        className="nav__link"
        exact
        to="/home"
        activeClassName="nav__link--active"
      >
        Home
      </NavLink>
      <NavLink
        className="nav__link"
        exact
        to="/favorites"
        activeClassName="nav__link--active"
      >
        Favorites
      </NavLink>
    </nav>
  );
};

export default Nav;
