import React from 'react';
import AppTitle from '../../atoms/AppTitle/AppTitle';
import HeaderActions from '../../molecules/HeaderActions/HeaderActions';
import { TOKEN } from '../../../constants';
import Nav from '../../molecules/Nav/Nav';
import './Header.scss';

const Header = () => {
  return (
    <header className="header">
      <div className="header__main">
        <AppTitle />
        {TOKEN && <HeaderActions />}
      </div>
      {TOKEN && <Nav />}
    </header>
  );
};

export default Header;
