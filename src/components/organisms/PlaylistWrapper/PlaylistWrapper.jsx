import React, { useEffect, useState } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import { getTracksFromPlaylist } from '../../../services/spotifyApi';
import LazyTrack from '../../molecules/TrackCard/LazyTrack';
import './PlaylistWrapper.scss';

const PlaylistWrapper = ({ id, name }) => {
  const [playlistTracks, setPlaylistTracks] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  let { url } = useRouteMatch();

  useEffect(() => {
    const requestTracksFromPlaylist = async () => {
      const response = await getTracksFromPlaylist(id);
      setPlaylistTracks(response);
      setIsLoading(false);
    };
    requestTracksFromPlaylist();
  }, [id]);

  return (
    <section className="playlist">
      {isLoading || (
        <>
          <h3 className="playlist__name">{name}</h3>
          {playlistTracks.map((track, index) => {
            return (
              index < 5 && (
                <LazyTrack key={index} track={track.track || false} />
              )
            );
          })}
          <Link className="playlist__link" to={`${url}/:${id}`}>
            See all songs of {name}
          </Link>
        </>
      )}
    </section>
  );
};

export default PlaylistWrapper;
