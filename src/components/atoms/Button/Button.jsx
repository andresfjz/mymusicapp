import React from 'react';
import './Button.scss';

const PrimaryButton = ({
  myStyle = 'primary',
  action = undefined,
  type = 'button',
  text = '',
}) => {
  return (
    <button
      className={`button button--${myStyle}`}
      onClick={action}
      type={type}
    >
      {text}
    </button>
  );
};

export default PrimaryButton;
