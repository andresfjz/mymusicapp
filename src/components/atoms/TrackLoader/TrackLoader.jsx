import React from 'react';
import ContentLoader from 'react-content-loader';

const MyLoader = props => (
  <ContentLoader
    speed={2}
    width={300}
    height={250}
    viewBox="0 0 300 250"
    backgroundColor="#f2f2f2"
    foregroundColor="#ecebeb"
    {...props}
  >
    <rect x="2%" y="0" rx="1rem" ry="1rem" width="96%" height="6rem" />
    <rect x="15%" y="6.5rem" rx="0" ry="0" width="70%" height="1rem" />
    <rect x="15%" y="8rem" rx="0" ry="0" width="70%" height="1rem" />
    <rect x="15%" y="9.5rem" rx="0" ry="0" width="70%" height="1rem" />
    <rect x="10%" y="11rem" rx="0" ry="0" width="35%" height="3rem" />
    <rect x="55%" y="11rem" rx="0" ry="0" width="35%" height="3rem" />
    <rect x="15%" y="14.5rem" rx="0" ry="0" width="70%" height=".9rem" />
  </ContentLoader>
);

export default MyLoader;
