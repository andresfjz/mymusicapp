import React from 'react';
import { TOKEN } from '../../../constants';
import './AppTitle.scss';

const AppTitle = () => {
  return (
    <a className="title__link" href={(TOKEN && '/home') || '/'}>
      <h1 className="title__text">My Music App 🎶</h1>
    </a>
  );
};

export default AppTitle;
