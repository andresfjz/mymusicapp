import React from 'react';

const AuthInput = ({ className = '', type = 'text', placeholder = '' }) => {
  return <input className={className} type={type} placeholder={placeholder} />;
};

export default AuthInput;
