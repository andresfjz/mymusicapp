/* My Music App */
export const MY_SPOTIFY_ID = '35r2yve3f8a2tqvuolmrmuvmh';

/* Spotify Authentication */
export const SPOTIFY_AUTHORIZE_ENDPOINT =
  'https://accounts.spotify.com/authorize';
export const CLIENT_ID = 'ee13393ab78449b2b893ee30d88d32e4';
export const CLIENT_SECRET = '0201b3877a34479d817fce382daaec7a';
export const REDIRECT_URL = 'http://localhost:3000/home';
export const SCOPES = [
  'user-top-read',
  'user-read-private',
  'user-library-read',
  'user-library-modify',
  'playlist-read-private',
].join('%20');
export const RESPONSE_TYPE = 'token';
export const SHOW_DIALOG = 'true';

/* Token */
export const TOKEN = localStorage.getItem('token') || false;

/* Spotify API Endpoints */
export const PROFILE_DATA_ENDPOINT = 'https://api.spotify.com/v1/me';
export const PLAYLIST_ENDPOINT = 'https://api.spotify.com/v1/playlists';
export const TRACKS_ENDPOINT = 'https://api.spotify.com/v1/me/tracks';
export const USER_PLAYLISTS_ENDPOINT =
  'https://api.spotify.com/v1/users/{user_id}/playlists';
export const PLAYLIST_TRACKS_ENDPOINT =
  'https://api.spotify.com/v1/playlists/{playlist_id}/tracks?market=co';
export const IS_TRACK_IN_USER_LIBRAY_ENDPOINT =
  'https://api.spotify.com/v1/me/tracks/contains';

/* Pattern (Form Validations) */
export const NAME_PATTERN = /^[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]+$/;
export const EMAIL_PATTERN = /^(\w+[/./-]?){1,}@[a-z]+[/.]\w{2,}$/;
export const PASSWORD_PATTERN =
  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/;
