import React from 'react';
import RegisterForm from '../../components/molecules/RegisterForm/RegisterForm';
import './Register.scss';

const Register = () => {
  return (
    <main className="register">
      <RegisterForm />
    </main>
  );
};

export default Register;
