import React, { useEffect, useState } from 'react';
import Loader from '../../components/atoms/Loader/Loader';
import PlaylistWrapper from '../../components/organisms/PlaylistWrapper/PlaylistWrapper';
import { MY_SPOTIFY_ID, TOKEN } from '../../constants';
import { getSpecificUserPlaylists } from '../../services/spotifyApi';
import './MyMusic.scss';

const MyMusic = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [myPlaylists, setMyPlaylists] = useState([]);

  !TOKEN && window.location.assign('/');

  useEffect(() => {
    const requestSpecificUserPlaylists = async () => {
      const { items } = await getSpecificUserPlaylists(MY_SPOTIFY_ID);
      setMyPlaylists(items);
      setIsLoading(false);
    };
    requestSpecificUserPlaylists();
  }, []);

  return (
    <main className="mymusic">
      <p className="mymusic__text">
        Here you can find my favorite Spotify playlists! Stay tuned, sometimes
        they update 😜
      </p>
      {(isLoading && <Loader />) || (
        <div className="mymusic__playlist">
          {myPlaylists.map((playlist, index) => {
            return (
              <PlaylistWrapper
                key={index}
                id={playlist.id}
                name={playlist.name}
                tracks={playlist.tracks}
              />
            );
          })}
        </div>
      )}
    </main>
  );
};

export default MyMusic;
