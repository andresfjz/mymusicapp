import React from 'react';
import LoginForm from '../../components/molecules/LoginForm/LoginForm';
import './Login.scss';

const Login = () => {
  return (
    <main className="login">
      <p className="login__text">
        Welcome to My Music App, explore my favorite playlists, songs, and if
        you like it you can add them to your Spotify library!
      </p>
      <div className="login__divider"></div>
      <LoginForm />
    </main>
  );
};

export default Login;
