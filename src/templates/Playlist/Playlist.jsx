import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { getPlaylist, getTracksFromPlaylist } from '../../services/spotifyApi';
import { Link } from 'react-router-dom';
import './Playlist.scss';
import Loader from '../../components/atoms/Loader/Loader';
import LazyTrack from '../../components/molecules/TrackCard/LazyTrack';

const Playlist = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [playlistFound, setPlaylistFound] = useState(false);

  let { id } = useParams();
  id = id.substr(1);

  const [playlistName, setPlaylistName] = useState('');
  const [playlistTracks, setPlaylistTracks] = useState([]);

  useEffect(() => {
    const requestTracksFromPlaylist = async () => {
      const response = await getTracksFromPlaylist(id);
      setPlaylistTracks(response);
      setIsLoading(false);
    };

    const requestPlaylist = async () => {
      const response = await getPlaylist(id);
      setPlaylistName(response?.name);
      if (response?.name) {
        setPlaylistFound(true);
        requestTracksFromPlaylist();
      }
    };

    requestPlaylist();
  }, [id]);

  if (!playlistFound) {
    return (
      <>
        {(isLoading && <Loader />) || (
          <main className="playlist">
            <h2 className="playlist__text playlist__text--title">
              No playlist with that id!
            </h2>
            <Link className="playlist__link" to="/home">
              Go back to Home
            </Link>
          </main>
        )}
      </>
    );
  }
  return (
    <main className="playlist">
      {(isLoading && <Loader />) || (
        <>
          <h2 className="playlist__text playlist__text--title">
            {playlistName}
          </h2>
          <div className="playlist__tracks">
            {playlistTracks.map((track, index) => {
              return <LazyTrack key={index} track={track.track || false} />;
            })}
          </div>
        </>
      )}
    </main>
  );
};

export default Playlist;
