import React, { useState, useEffect } from 'react';
import Loader from '../../components/atoms/Loader/Loader';
import { TOKEN } from '../../constants';
import { getUserSavedTracks } from '../../services/spotifyApi';
import './Favorites.scss';
import LazyTrack from '../../components/molecules/TrackCard/LazyTrack';

const Favorites = () => {
  !TOKEN && window.location.assign('/');

  const [isLoading, setIsLoading] = useState(true);
  const [userSavedTracks, setUserSavedTracks] = useState([]);

  useEffect(() => {
    const requestUserSavedTracks = async () => {
      const userSavedTracks = await getUserSavedTracks();
      setUserSavedTracks(userSavedTracks.items);
      setIsLoading(false);
    };
    requestUserSavedTracks();
  }, []);

  return (
    <main className="favorites">
      <h2 className="favorites__text">
        {(userSavedTracks.length > 0 && 'There are your favorite songs:') ||
          (!isLoading && "You don't have favorite songs")}
      </h2>
      <div className="favorites__tracks">
        {(isLoading && <Loader />) || (
          <>
            {userSavedTracks.map((item, index) => {
              return <LazyTrack key={index} track={item.track} />;
            })}
          </>
        )}
      </div>
    </main>
  );
};

export default Favorites;
