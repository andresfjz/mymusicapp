import React from 'react';

const Error404 = () => {
  return (
    <main className="404__main">
      <h2 className="404__title">Error 404</h2>
      <p className="404__text">Page not found!</p>
    </main>
  );
};

export default Error404;
