import React, { useEffect } from 'react';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';
import '../App.scss';
import MyMusic from '../templates/MyMusic/MyMusic';
import { saveMyToken } from '../services/spotifyAuth';
import { TOKEN } from '../constants';
import Playlist from '../templates/Playlist/Playlist';

const Home = () => {
  !TOKEN && window.location.assign('/');

  useEffect(() => {
    if (window.location.hash) {
      saveMyToken(window.location.hash);
      window.location.replace('/home');
    }
  }, []);

  let { path } = useRouteMatch();
  return (
    <Switch>
      <Route exact path={path} component={MyMusic} />
      <Route path={`${path}/:id`} component={Playlist} />
      <Route path={`${path}/*`}>
        <Redirect to={path} />
      </Route>
    </Switch>
  );
};

export default Home;
