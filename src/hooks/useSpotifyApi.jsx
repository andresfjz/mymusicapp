import axios from 'axios';
import { useState, useEffect } from 'react';
import { TOKEN } from '../constants';

export const useSpotifyApi = (endpoint, method) => {
  const [data, setData] = useState();
  const [pending, setPending] = useState(true);

  useEffect(() => {
    const header = { Authorization: 'Bearer ' + TOKEN };

    const getData = async (endpoint, method) => {
      try {
        let response;
        switch (method) {
          case 'GET':
            response = await axios.get(endpoint, { headers: header });
            break;
          default:
            console.log('Invalid method!');
            break;
        }
        if (response.status === 200) {
          setPending(false);
          setData(response.data);
        }
      } catch (error) {
        console.log(error);
      }
    };
    getData(endpoint, method);
  }, [endpoint, method]);

  return { data, pending };
};
