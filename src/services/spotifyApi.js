import axios from 'axios';
import {
  TOKEN,
  /* PROFILE_DATA_ENDPOINT, */
  TRACKS_ENDPOINT,
  USER_PLAYLISTS_ENDPOINT,
  PLAYLIST_TRACKS_ENDPOINT,
  IS_TRACK_IN_USER_LIBRAY_ENDPOINT,
  PLAYLIST_ENDPOINT,
} from '../constants';

const myHeader = {
  Authorization: 'Bearer ' + TOKEN,
};

/* Obtener datos del perfil del usuario */
// export const getUserProfileData = async () => {
//   try {
//     const response = await axios.get(PROFILE_DATA_ENDPOINT, {
//       headers: myHeader,
//     });
//     return response.data;
//   } catch (error) {
//     console.log(error);
//   }
// };

/* Obtener una playlist */
export const getPlaylist = async id => {
  try {
    const response = await axios.get(`${PLAYLIST_ENDPOINT}/${id}`, {
      headers: myHeader,
    });
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

/* Obtener las canciones guardadas del usuario */
export const getUserSavedTracks = async () => {
  try {
    const response = await axios.get(TRACKS_ENDPOINT, { headers: myHeader });
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

/* Remover una canción guardada por el usuario */
export const removeTrackFromUserLibrary = async id => {
  try {
    const response = await axios.delete(`${TRACKS_ENDPOINT}?ids=${id}`, {
      headers: myHeader,
    });
    return response;
  } catch (error) {
    console.log(error);
  }
};

/* Agregar una canción a la biblioteca del usuario */
export const addTrackToUserLibrary = async id => {
  try {
    const response = await fetch(`${TRACKS_ENDPOINT}?ids=${id}`, {
      method: 'PUT',
      headers: myHeader,
    });
    return response;
  } catch (error) {
    console.log(error);
  }
};

/* Obtener las playlists guardadas de un usuario especifico */
export const getSpecificUserPlaylists = async id => {
  try {
    const response = await axios.get(
      `${USER_PLAYLISTS_ENDPOINT.replace('{user_id}', id)}`,
      {
        headers: myHeader,
      }
    );
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

/* Obtener canciones de una playlist */
export const getTracksFromPlaylist = async id => {
  try {
    const response = await axios.get(
      `${PLAYLIST_TRACKS_ENDPOINT.replace('{playlist_id}', id)}`,
      {
        headers: myHeader,
      }
    );
    return response.data.items;
  } catch (error) {
    console.log(error);
  }
};

/* Verificar si el usuario tiene una cancion en su biblioteca */
export const checkIsTrackInUserLibrary = async id => {
  try {
    const response = await axios.get(
      `${IS_TRACK_IN_USER_LIBRAY_ENDPOINT}?ids=${id}`,
      {
        headers: myHeader,
      }
    );
    return response.data[0];
  } catch (error) {
    console.log(error);
  }
};
