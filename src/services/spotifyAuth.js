import {
  SPOTIFY_AUTHORIZE_ENDPOINT,
  CLIENT_ID,
  CLIENT_SECRET,
  REDIRECT_URL,
  SCOPES,
  RESPONSE_TYPE,
  SHOW_DIALOG,
} from '../constants';

export const handleAuth = () => {
  window.location = `${SPOTIFY_AUTHORIZE_ENDPOINT}?client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}&redirect_uri=${REDIRECT_URL}&scope=${SCOPES}&response_type=${RESPONSE_TYPE}&show_dialog=${SHOW_DIALOG}`;
};

const getParamsFromHash = hash => {
  const hashContent = hash.substr(1);
  const paramsSplit = hashContent.split('&');

  let params = {};
  let values = [];

  paramsSplit.forEach(param => {
    values = param.split('=');
    params[values[0]] = values[1];
  });

  return params;
};

export const saveMyToken = hash => {
  const hashParams = getParamsFromHash(hash);
  localStorage.setItem('token', hashParams.access_token);
  window.history.pushState({}, null, '/home');
};

export const deleteMyToken = () => {
  localStorage.removeItem('token');
  window.location.href = '/';
};
